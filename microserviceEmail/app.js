const express = require("express");
const router = express.Router();
const amqp = require("amqplib");
const axios = require("axios");
const nodemailer = require("nodemailer");
require("express-async-errors");

const { EMAIL, PASSWORD, RABBITMQ_UR, PORT} = require("./src/config/index");

const sendEmail = async (mail) => {
    try {
      const transporter = nodemailer.createTransport({
        service: "gmail",
        port: 465,
        secure: false, // beacause we're not using https
  
        logger: true,
        debug: true,
        secureConnection: false,
        auth: {
          user: process.env.EMAIL,
          pass: process.env.PASSWORD,
        },
        tls:{
          rejectUnauthorized: true
        }
      });
  
      const mailOptions = {
        from: process.env.EMAIL,
        to: mail,
        subject: 'Payment confirmation',
        text: 'Your payment has been successfully completed',
      };
  
      const info = await transporter.sendMail(mailOptions);
      console.log('Mail sent successfully: ', info.response);
      return info;
    } catch (error) {
      console.error('Error while sending email', error.message);
      throw error;
    }
  };

var channel;

async function connectRabbitMQ() {
    try {
      const amqpServer = process.env.RABBITMQ_URL || "amqp://localhost:5672";
      const connection = await amqp.connect(amqpServer);
      channel = await connection.createChannel();
      await channel.assertQueue("EMAIL");
  
      channel.consume("EMAIL", (data) => {
        try {
          console.log(data.content)
          const { email } = JSON.parse(data.content);
          console.log("Consuming EMAIL queue  " + email);
          sendEmail(email);
          channel.ack(data);
        } catch (error) {
          console.error('Error while consuming message: ', error);
        }
      });
  
      console.log("Connected to RabbitMQ");
    } catch (error) {
      console.error("Error while connecting to RabbitMQ", error);
      process.exit(1);
    }
  }

  async function start() {
    await connectRabbitMQ();
  
    const app = express();
    app.use(express.json());
  
    const port = PORT;
    app.listen(port, () => {
      console.log(`Email Service listening on port  ${port}`);
    });
  }
  
  start();