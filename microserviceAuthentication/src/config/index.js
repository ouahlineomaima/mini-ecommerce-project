const dotEnv = require("dotenv");


const configFile = `./.env.dev`;
dotEnv.config({ path: configFile });
 

const { JWT_SECRET, PORT, DB_URL } = process.env;


module.exports = {
    JWT_SECRET, PORT, DB_URL
};
