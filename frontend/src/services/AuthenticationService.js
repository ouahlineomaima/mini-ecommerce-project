import axios from "axios";


const API_BASE_URL = "http://localhost:3001/users";

const setAuthorizationHeader = (token) => {
  if (!token || typeof token !== 'string') {
    throw new Error("Invalid token");
  }
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
};

const AuthenticationService = {
  isAuthenticated: () => {
    const accessToken = localStorage.getItem("accessToken");
    const userId = localStorage.getItem("userId");

    return !!accessToken && !!userId;
  },

  register: async (userData) => {
    try {
      const response = await axios.post(`${API_BASE_URL}/register`, userData);
      return response.data;
    } catch (error) {
      console.error("Error in register:", error);
      throw error.response.data;
    }
  },

  login: async (credentials) => {
    try {
      const response = await axios.post(`${API_BASE_URL}/login`, credentials);
      const { accessToken, userId } = response.data;
      setAuthorizationHeader(accessToken);
      localStorage.setItem("accessToken", accessToken);
      localStorage.setItem("userId", userId);
  
      return { accessToken, userId };
    } catch (error) {
      console.error("Error in login:", error);
      throw error.response.data;
    }
  },
  
  getUser: async (userId) => {
    try {
      if (!AuthenticationService.isAuthenticated()) {
        throw new Error("User not authenticated");
      }

      console.log("User id", userId);
      const response = await axios.get(`${API_BASE_URL}/${userId}`);
      return response.data;
    } catch (error) {
      if (error.message === "User not authenticated") {
        console.warn("User not authenticated");
      } else {
        throw error.response.data;
      }
    }
  },
  
  logout: () => {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("userId");
    delete axios.defaults.headers.common["Authorization"];
  },

  setUserToken: (token) => {
    try {
      setAuthorizationHeader(token);
      localStorage.setItem("accessToken", token);

      console.log("User token set successfully:", token);
    } catch (error) {
      console.error("Error setting user token:", error.message);
    }
  },
  
};

export default AuthenticationService;