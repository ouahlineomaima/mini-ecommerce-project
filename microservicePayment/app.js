const express = require("express");
const cors = require('cors');
const app = express();
const paiementRoutes = require("./src/controllers/paymentController");
const { connectToDatabase } = require("./src/config/paymentDatabase");

// Connect to the database
connectToDatabase();

app.use(cors());
app.use(express.json());
app.use("/", paiementRoutes);

const port = process.env.PORT || 5002;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
