const express = require("express");
const router = express.Router();
const orderService = require("../services/orderService");

router.post("/orders", async (req, res) => {
  const { productId, orderDate, quantity, orderPay } = req.body;
  try {
    const newOrder = await orderService.createOrder({
      productId,
      orderDate,
      quantity,
      orderPay,
    });

    res.status(201).json(newOrder);
  } catch (err) {
    console.error(err.message);
    res.status(500).send(err.message);
  }
});

router.get("/orders/:id", async (req, res) => {
  try {
    const orderId = req.params.id;
    const order = await orderService.getOrderById(orderId);
    res.json(order);
  } catch (err) {
    console.error(err.message);
    res.status(500).send(err.message);
  }
});
router.get("/orders", async (req, res) => {
  try {
    const orders = await orderService.getOrders();
    res.json(orders);
  } catch (err) {
    console.error(err.message);
    res.status(500).send(err.message);
  }
});

router.put("/orders/:id", async (req, res) => {
  try {
    const orderId = req.params.id;
    const { productId, orderDate, quantity, orderPay } = req.body;

    const updatedOrder = await orderService.updateOrder(orderId, {
      productId,
      orderDate,
      quantity,
      orderPay,
    });

    res.json(updatedOrder);
  } catch (err) {
    console.error(err.message);
    res.status(500).send(err.message);
  }
});

module.exports = router;
