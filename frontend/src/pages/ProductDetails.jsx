import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router';
import OrderButton from '../components/OrderButton';
import ProductService from '../services/ProductService';
import OrderService from '../services/OrderService';

export default function ProductDetails() {
    const { productId } = useParams();
    const [product, setProduct] = useState(null);
    const [error, setError] = useState(null);
    const [quantity, setQuantity] = useState(0);

    const handleOrderButtonClick = async () => {
        if (productId && quantity > 0) {
            try {
              const response = await OrderService.placeOrder(
                productId,
                new Date(), 
                quantity,
                false 
              );
              console.log(response)
              const orderId = response._id;
              console.log('Order placed with ID:', orderId);
      
              
              setQuantity(0);

              
            } catch (error) {
              console.error(error.message);
            }
          } else {
            console.error('Invalid productId or quantity. Cannot place the order.');
            setError('Invalid productId or quantity. Cannot place the order.')
          }
    };

    const handleQuantityChange = (newQuantity) => {
        setQuantity(newQuantity);
    };
    
    useEffect(() => {
        const fetchProduct = async () => {
            try {
                
                const productData = await ProductService.getProduct(productId);
                setProduct(productData);
                setError(null);
            } catch (error) {
                console.error('Error fetching product details:', error);
                setError('An error occurred while fetching product details.');
            }
        };
        

        fetchProduct();
    }, [productId]);

    return (
        <div className='w-screen min-h-screen h-max bg-zinc-900 flex flex-col gap-8 items-center justify-center'>
            {product && !error && (
                <>
                <h1 className='font-montserrat font-bold text-bkg text-xl '>Discover our magnificant {product.titre}</h1>
                <div className='flex flex-row w-3/4 h-max gap-6 bg-content shadow-lg rounded'>
                    <img src={product.image} alt={product.titre} className='w-2/3 h-full py-4 rounded-[1rem] pl-4' />
                    <div className='flex flex-col w-1/3 justify-center items-start gap-4'>
                        <h1 className='font-nunito-sans font-bold text-bkg text-3xl uppercase'>{product.titre}</h1>
                        <p className='text-bkg text-sm font-normal font-nunito-sans'>{product.description}</p>
                        <h2 className='text-red-400 text-2xl font-bold font-nunito-sans'>{product.price} MAD</h2>
                        <OrderButton
                            quantity={quantity}
                            onQuantityChange={handleQuantityChange}
                            onOrderButtonClick={handleOrderButtonClick}
                        />
                    </div>
                </div>
                </>
            )}
            {error  && (
                <h1 className='font-montserrat font-bold text-bkg text-xl'>{error}</h1>
            )}
        </div>
    );
}
