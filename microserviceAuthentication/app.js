const express = require("express");
const mongoose = require("mongoose");
const app = express();
const userRouter = require("./src/router/userRouter");
const cors = require('cors');

const { JWT_SECRET, PORT, DB_URL} = require("./src/config/index");

mongoose.connect(DB_URL, {
    useUnifiedTopology: true,
});
app.use(cors());

const db = mongoose.connection;

db.on("error", (error) => {
    console.error("Failed to connect to the database:", error);
    process.exit(1); 
});

db.once("open", function () {
    console.log("Database connected successfully");
});

app.use(express.json());
app.use("/users", userRouter);

const port = PORT || 5004;
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});