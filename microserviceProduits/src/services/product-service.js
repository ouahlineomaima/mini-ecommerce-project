const { ProductRepository } = require("../database/repository/product-repository");

class ProductService{

    constructor(){
        this.repository = new ProductRepository();
    }

    async GetProducts(){
        const productResult = await this.repository.GetProducts();
        
        return productResult;
    }

    async GetProductById(id){
        const product = await this.repository.GetProductsById(id);
        return product;
    }
}
module.exports = {ProductService};