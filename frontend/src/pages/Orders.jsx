import React from 'react'
import { useEffect, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import OrderService from '../services/OrderService';
import ProductService from '../services/ProductService';
import PaymentService from '../services/PaymentService';
import AuthenticationService from '../services/AuthenticationService';

function createData(
    orderId: String,
    productName: string,
    orderDate: Date,
    quantity: number,
    unitPrice: Number,
    total: number,
    isPayed: Boolean
) {
    return { orderId, productName, orderDate: orderDate.toLocaleDateString(), quantity, unitPrice, total, isPayed };
}


export default function Orders() {
    const [orders, setOrders] = useState(null);
    const [error, setError] = useState(null);
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);

    const fetchOrders = async () => {
        
        try {
            const orders = await OrderService.getOrders();
            if (orders) {
                const productDetails = await Promise.all(orders.map(order => ProductService.getProduct(order.productId)));
                const newRows = orders.map((order, index) => createData(
                    order._id,
                    productDetails[index].titre,
                    new Date(order.orderDate),
                    order.quantity,
                    productDetails[index].price,
                    order.quantity * productDetails[index].price,
                    order.orderPay
                ));
                setRows(newRows);
                setOrders(orders);
                setError(null);
            }
        }
        catch (error) {
            console.error('Error fetching product details:', error);
            setError('An error occurred while fetching product details.');
        }
    };

    useEffect(() => {
        fetchOrders();
    }, []);

    const handlePayment = async (orderId, total) => {
        const user = await AuthenticationService.getUser(localStorage.getItem('userId'))
        setLoading(true)
        try {
            const paymentData = {
                idCommande: orderId,
                montant: total,
                email: user.email

            };
            console.log(paymentData)
            const paymentResponse = await PaymentService.makePayment(paymentData);
            setLoading(true);
            fetchOrders();
            setLoading(false);
        } catch (error) {
            console.error('Error making payment:', error.message);
            setError('An error occurred while processing payment.');
        }
        finally {
            setLoading(false)
        }
    }


    return (
        <div className='flex flex-col w-screen h-screen bg-zinc-900 items-center gap-4 pt-2'>

            <h1 className='font-montserrat font-bold text-bkg text-xl '>Orders Overview</h1>
            {error && !orders && (
            <h1 className='font-montserrat text-3xl text-bkg pt-5'>{error}</h1>
        )}
            {loading && (
                <div className='w-[20rem] h-[10rem] flex items-center justify-center shadow-lg bg-content rounded-lg'>
                    <h1 className='text-xl font-poppins text-bkg text-center'>We're processing the payment. Please hold on.</h1>

                </div>
            )}
            {!error && (
                <TableContainer component={Paper} style={{ background: 'black' }}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left" style={{ color: 'white' }}>Product name</TableCell>
                            <TableCell align="left" style={{ color: 'white' }}>Order Date</TableCell>
                            <TableCell align="left" style={{ color: 'white' }}>Quantity</TableCell>
                            <TableCell align="left" style={{ color: 'white' }}>Unit price</TableCell>
                            <TableCell align="left" style={{ color: 'white' }}>Total amount</TableCell>
                            <TableCell align="left" style={{ color: 'white' }}>Pay</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                            <TableRow
                                key={row.name}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell align="left" style={{ color: 'white' }}>{row.productName}</TableCell>
                                <TableCell align="left" style={{ color: 'white' }}>{row.orderDate}</TableCell>
                                <TableCell align="left" style={{ color: 'white' }}>{row.quantity}</TableCell>
                                <TableCell align="left" style={{ color: 'white' }}>{row.unitPrice}</TableCell>
                                <TableCell align="left" style={{ color: 'white' }}>{row.total}</TableCell>
                                <TableCell align="center" style={{ color: 'white' }}>
                                    <button
                                        className={`flex w-[4.36781rem] h-[2.42938rem] ${!row.isPayed ? 'bg-red-400' : 'bg-green-400'} justify-center items-center shadow-md rounded-xl`}
                                        onClick={() => handlePayment(row.orderId, row.total)}
                                        disabled={row.isPayed}
                                    >
                                        {!row.isPayed ? 'Pay' : 'Paid'}
                                    </button>

                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

            )}
            

        </div>

    );
}
