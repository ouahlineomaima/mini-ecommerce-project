const express = require("express");
const router = express.Router();
const paymentService = require("../services/paymentService");



router.post("/payments", async (req, res) => {
  try {
    const paiementData = req.body;
    
    const result = await paymentService.processPayment(paiementData);
    res.status(result.status).json(result.data);
  } catch (err) {
    console.error(err);

    if (err.response) {
      res.status(err.response.status).json({ error: err.response.data });
    } else if (err.request) {
      res.status(500).json({ error: "No response received from the server." });
    } else {
      res.status(500).json({ error: err.message || "Internal Server Error" });
    }
  }
});

module.exports = router;
