import axios from 'axios';

const API_BASE_URL = 'http://localhost:3001/orders'; 


const OrderService = {
  placeOrder: async (productId, orderDate, quantity, orderPay ) => {
    try {
      const response = await axios.post(`${API_BASE_URL}`, {
        productId,
        orderDate,
        quantity,
        orderPay,
      });

      return response.data;
    } catch (error) {
      throw new Error(`Error placing order: ${error.message}`);
    }
   
  },

  getOrderById: async (orderId) => {
    try {
        const response = await axios.get(`${API_BASE_URL}/${orderId}`);
        return response.data;
      } catch (error) {
        throw new Error(`Error getting order: ${error.message}`);
      }
    
  },

  getOrders: async () =>{
    try {
        const response = await axios.get(`${API_BASE_URL}`);
        return response.data;
      } catch (error) {
        throw new Error(`Error getting order: ${error.message}`);
      }

  },

  updateOrder: async (orderId, { productId, orderDate, quantity, orderPay }) => {
    
  },
};

export default OrderService;
