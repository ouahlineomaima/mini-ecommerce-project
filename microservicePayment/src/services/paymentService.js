const axios = require("axios");
const Paiement = require("../models/paymentModel");
const amqp = require('amqplib');

// connection to message broker
var channel, connection;

async function connect() {
  const amqpServer = "amqp://localhost:5672";
  connection = await amqp.connect(amqpServer);
  channel = await connection.createChannel();
  await channel.assertQueue("EMAIL");

}

connect();

async function processPayment(paiementData) {
  try {
    console.log('sss', paiementData)
    const existingPaiement = await Paiement.findByIdCommande(paiementData.idCommande);


    if (existingPaiement) {
      console.log("Payment already exists for this order. Aborting.");
      return { status: 409, data: { error: "Cette commande est déjà payée" } };
    }

    const newPaiement = await Paiement.create(paiementData);

    if (!newPaiement) {
      console.error("Failed to create new payment record.");
      return {
        status: 500,
        data: { error: "Erreur, impossible d'établir le paiement, réessayez plus tard" },
      };
    }

    console.log("New payment created:", newPaiement);

    const commandeResponse = await axios.get(
      `http://localhost:5001/orders/${newPaiement.idCommande}`
    );
    const commandData = commandeResponse.data;

    if (!commandData) {
      console.error("No data found for the corresponding order.");
      return {
        status: 404,
        data: { error: "La commande correspondante n'a pas été trouvée" },
      };
    }

    commandData.orderPay = true;

    const updateCommandeResponse = await axios.put(
      `http://localhost:5001/orders/${newPaiement.idCommande}`,
      commandData
    );

    if (updateCommandeResponse.status !== 200) {
      console.error("Failed to update order status:", updateCommandeResponse.statusText);
      return { status: 500, data: { error: "Erreur lors de la mise à jour de la commande" } };
    }
    console.log(paiementData)

    const email = paiementData.email;
    console.log("Sending email to: " + email);
    channel.sendToQueue(
      "EMAIL",
      Buffer.from(
        JSON.stringify({
          email,
        })
      )
    );



    console.log("Payment processed successfully.");
    return { status: 201, data: newPaiement };
  } catch (err) {
    console.error("An error occurred during payment processing:", err);
    return { status: 500, data: { error: "Une erreur est survenue lors du paiement" } };
  }
}

module.exports = { processPayment };
