import React from 'react'
import { Link } from 'react-router-dom'

export default function Product({ product }) {
    return (
        <div className="w-[12.375rem] h-[14.188rem] px-2.5 pt-2.5 pb-[15px] bg-content rounded-[10px] flex-col justify-start items-start gap-2 inline-flex shadow-md" >
            <Link to={`/products/${product._id}`}>
                <div className="w-[11.125rem] h-[8.188rem] rounded-[5px]">
                    <img src={`${product.image}`} alt={product.titre} className='w-full h-full rounded'/>
                </div>
                <div className="h-max flex-col justify-start items-start gap-[5px] flex ">
                    <div className="self-stretch h-max text-bkg text-lg font-medium font-sans">{product.titre}</div>
                    
                    <div className="w-[7.188rem] h-[3.063rem] text-red-400 text-lg font-bold font-inter">{product.price} MAD</div>
                </div>
            </Link>


        </div>
    )
}
