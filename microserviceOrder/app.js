const express = require("express");
const cors = require('cors');
const app = express();
const orderRoutes = require("./src/controllers/OrderController");
const { connectToOrderDatabase } = require("./src/config/orderDatabase");

// Connect to the order database
connectToOrderDatabase();

app.use(cors());
app.use(express.json());
app.use("/", orderRoutes);

const port = process.env.PORT || 5001;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
