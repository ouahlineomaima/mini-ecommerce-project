import './App.css';
import NavBar from './components/NavBar';
import { Navigate } from 'react-router-dom';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import ProductDetails from './pages/ProductDetails';
import Orders from './pages/Orders';
import { useState } from 'react';
import AuthenticationService from './services/AuthenticationService';
import Login from './pages/Login';
import Register from './pages/Register';
//import PrivateRoute from './components/PrivateRoute';


const PrivateRoute = ({ element, isAuthenticated, onlogin }) => {
  return isAuthenticated ? (
    element
  ) : (
    <Navigate to="/login" replace state={{ from: window.location.pathname }} />
  );
};

function App() {

  const [isAuthenticated, setIsAuthenticated] = useState(false)
  const handleLogout = () => {
    AuthenticationService.logout();
    setIsAuthenticated(false);
  };

  const handleLogin = (status, email) => {

    setIsAuthenticated(status);
  };

  return (
    <Router>
      <NavBar isAuthenticated={isAuthenticated} onLogout={handleLogout}></NavBar>
      <Routes>
        {/* <Route path="/" element={<Home />} /> */}
        <Route path="/login" element={<Login onLogin={handleLogin} />} />
        <Route path="/register" element={<Register />} />
        {/* <Route path="/products" element={<Home />} />
        <Route path="/products/:productId" element={<ProductDetails />} />
        <Route path="/orders" element={<Orders />} /> */}

        <Route
          path="/"
          element={<PrivateRoute element={<Home />} isAuthenticated={isAuthenticated} />}
        />
        <Route
          path="/products"
          element={<PrivateRoute element={<Home />} isAuthenticated={isAuthenticated} />}
        />
        <Route
          path="/products/:productId"
          element={<PrivateRoute element={<ProductDetails />} isAuthenticated={isAuthenticated} />}
        />
        <Route
          path="/orders"
          element={<PrivateRoute element={<Orders />} isAuthenticated={isAuthenticated}  />}
        />
      </Routes>





    </Router >
  );
}

export default App;
