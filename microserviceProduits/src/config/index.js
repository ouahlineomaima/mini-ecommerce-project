const dotEnv = require("dotenv");


const configFile = `./.env.dev`;
dotEnv.config({ path: configFile });
 


const { DB_URL, PORT, EXCHANGE_NAME } = process.env;


module.exports = {
  PORT,
  DB_URL,

  EXCHANGE_NAME,
};
