import React from 'react'
import logo from '../assets/images/logo.png'
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

export default function NavBar({ isAuthenticated, onLogout }) {
    const navigate = useNavigate()

    const handleRegister = () => {
        navigate('/register')
    }
    const handleLogin = () => {
        navigate('/login')
    }

    return (

        < div className="w-screen  px-16 py-5 bg-zinc-900 justify-between items-center flex flex-row" >
            <Link to={'/'}>
                <img src={logo} alt="" className='w-20 h-20' />
            </Link>

            <div className="justify-center items-center gap-10 flex">
                {isAuthenticated && (
                    <div className="h-6 justify-start items-start gap-10 flex">
                        <Link to={'/products'} className="text-center text-white text-base font-normal font-sans">Products</Link>
                        <Link to={'/orders'} className="text-center text-white text-base font-normal font-sans">Orders</Link>
                        <div className="justify-start items-start gap-5 flex">
                            <div className="px-5 py-2.5 bg-gray-700 rounded-xl justify-center items-center gap-2.5 flex">
                                <button className="text-center text-white text-base font-normal font-['Open Sans']" onClick={onLogout}>Log out</button>
                            </div>
                        </div>
                    </div>

                )}

                {!isAuthenticated && (
                    <>
                        <div className="justify-start items-start gap-5 flex">
                            <div className="px-5 py-2.5 bg-zinc-100 rounded-xl justify-center items-center gap-2.5 flex">
                                <button className="text-center text-black text-base font-normal font-['Open Sans']" onClick={() => handleRegister()}>Register</button>
                            </div>
                        </div>
                        <div className="justify-start items-start gap-5 flex">
                            <div className="px-5 py-2.5 bg-gray-700 rounded-xl justify-center items-center gap-2.5 flex">
                                <button className="text-center text-white text-base font-normal font-['Open Sans']" onClick={() => handleLogin()}>Login</button>
                            </div>
                        </div>
                    </>
                )}


                
            </div>
        </div>

    )
}
