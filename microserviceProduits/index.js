const express = require("express");
const mongoose = require("mongoose");
const app = express();
const cors = require('cors');

const { PORT, DB_URL, EXCHANGE_NAME} = require("./src/config/index");
console.log(DB_URL);


mongoose.connect(DB_URL, {
    useUnifiedTopology: true,
});


const db = mongoose.connection;
app.use(cors());
db.on("error", console.error.bind(console, "Failed to connect to the database."));
db.once("open", function(){
    console.log("Database connected successfully");
});

// Importer les routes
const productRoutes = require("./src/controller/productController");


// Utiliser les routes
app.use("/", productRoutes);

const port = PORT || 5000;
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});