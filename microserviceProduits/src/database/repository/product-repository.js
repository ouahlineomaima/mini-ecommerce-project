const mongoose = require('mongoose');
const { Product } = require("../models/Product");

class ProductRepository{
    constructor(){

    }

    async GetProducts(){
        try{
            const products = await Product.find({}).exec();
            return products;

        }
        catch (err) {
            console.log(err);
            
        }
        
    }

    async GetProductsById(id){
        const result = await Product.findById(id);
        if(!result) throw new Error("Product not found");
        return result;
        
    }

}

module.exports = {ProductRepository};