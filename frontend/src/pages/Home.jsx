import React from 'react'
import { useState, useEffect } from 'react';
import ProductList from '../components/ProductList'
import ProductService from '../services/ProductService';

export default function Home() {
    const [products, setProducts] = useState(null);
    const [error, setError] = useState(null);
    

    useEffect(() => {
        const fetchProducts = async () => {
          try {
            const productsData = await ProductService.getAllProducts();
            setProducts(productsData);
            setError(null);
          } catch (error) {
            console.error('Products fetching error', error);
            setError('An error happend while fetching products from the database. Please comme back later.');
          }
        };
    
        fetchProducts();
        console.log('products')
      }, []);
  return (
    <div className='w-screen min-h-screen bg-zinc-900 flex flex-col justify-center items-center gap-8'>
        <h1 className='font-montserrat text-3xl text-bkg pt-5'>The perfect place to buy High Tech gadgets</h1>
        {products && !error && (
            <ProductList products={products}></ProductList>
        )}
        {error && !products && (
            <h1 className='font-montserrat text-3xl text-bkg pt-5'>{error}</h1>
        )}

        
    </div>
  )
}
