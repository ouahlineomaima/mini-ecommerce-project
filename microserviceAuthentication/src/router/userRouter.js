const express = require("express");
const router = express.Router();
const { userRegister, loginUser, getUser, logoutUser } = require("../controller/userController");
const validateToken = require("../middleware/tokenVlidationMiddleware"); 

router.route("/register").post(userRegister);
router.route("/login").post(loginUser);
router.route("/:id").get(validateToken, getUser);
router.post("/logout", logoutUser);

module.exports = router;