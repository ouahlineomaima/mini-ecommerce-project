const { ProductService } = require("../services/product-service");
const express = require('express');
const router = express.Router();

const service = new ProductService();

router.get("/products", async (req, res, next) => {
  try {
    const data  = await service.GetProducts();

    res.status(200).json(data);

  } catch (err) {
    console.error("can't get products", err);
    res.status(500).send("Internal server error: can't get products");

  }


});

router.get("/products/:id", async (req, res, next) => {
  const productId = req.params.id;
  try {
    const data  = await service.GetProductById(productId);
    if (!data) {
      return res.status(404).json({ message: "Product not found" });

    }
    return res.status(200).json(data);
  } catch (error) {
    console.error("can't get product by id", error);
    return res.status(500).json({ error });
  }
});

module.exports = router;

