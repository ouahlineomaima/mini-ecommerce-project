const dotEnv = require("dotenv");


const configFile = `./.env.dev`;
dotEnv.config({ path: configFile });
 

const { EMAIL, PASSWORD, RABBITMQ_UR, PORT } = process.env;


module.exports = {
    EMAIL, PASSWORD, RABBITMQ_UR, PORT
};
