import React from 'react';
import Product from './Product';


export default function ProductList({products}) {
  

  return (
    <div className="w-2/3 h-max p-2.5  rounded-[10px] justify-center items-center gap-[9px] inline-flex flex-wrap">
      {products.map((product) => (
        <Product key={product.id} product={product} />
      ))}
    </div>
  );
}

