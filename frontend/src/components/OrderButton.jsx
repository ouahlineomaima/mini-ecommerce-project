import React, { useState } from 'react';

const OrderButton = ({ quantity, onQuantityChange, onOrderButtonClick }) => {
  const [successMessage, setSuccessMessage] = useState(null);

  const handleQuantityChange = (newQuantity) => {
    onQuantityChange(newQuantity);
  };

  const handleOrderButtonClick = async () => {
    try {
      if (onOrderButtonClick) {
        onOrderButtonClick();
        setSuccessMessage('Your order has been added successfully');
        onQuantityChange(0);
      }

      setTimeout(() => {
        setSuccessMessage(null);
      }, 5000);
    } catch (error) {
      console.error(error.message);
    }
  };

  return (
    <div className="flex flex-col items-center justify-center space-y-4 ">
      <div className="flex items-center space-x-4">
        <label htmlFor="quantity" className="text-bkg">Quantité:</label>
        <div className="flex items-center border border-r shadow-lg rounded overflow-hidden">
          <button
            className="px-2 py-1 bg-content hover:bg-gray-900 focus:outline-none"
            onClick={() => onQuantityChange(Math.max(quantity - 1, 1))}
          >
            -
          </button>
          <input
            type="number"
            id="quantity"
            name="quantity"
            min="1"
            value={quantity}
            onChange={handleQuantityChange(quantity)}
            className="px-2 py-1 w-12 text-center bg-content text-bkg focus:outline-none"
          />
          <button
            className="px-2 py-1 bg-content hover:opacity-80 focus:outline-none"
            onClick={() => onQuantityChange(quantity + 1)}
          >
            +
          </button>
        </div>
      </div>

      <button className='flex w-[8.36781rem] h-[2.42938rem] bg-red-400 justify-center items-center shadow-md rounded-xl' onClick={handleOrderButtonClick}> Order
      </button>
      {successMessage && (
        <p className="text-green-500 bg-green-100 p-2 rounded-md border border-green-300 text-center">
          {successMessage}
        </p>
      )}
    </div>
  );
};

export default OrderButton;
