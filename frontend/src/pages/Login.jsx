import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import AuthenticationService from "../services/AuthenticationService";

export default function Login({ onLogin }) {
    const [credentials, setCredentials] = useState({
        email: "",
        password: "",
    });
    const [error, setError] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();


    const handleChange = (e) => {
        const { name, value } = e.target;
        setCredentials((prevCredentials) => ({
            ...prevCredentials,
            [name]: value,
        }));
    };


    const handleSubmit = async (e) => {
        e.preventDefault();
        console.log(onLogin)

        try {
            setIsLoading(true);
            setError(""); 
            console.log("Credentials:", credentials);

            const { accessToken, userId } = await AuthenticationService.login(credentials);
            

            const user = await AuthenticationService.getUser(userId);

            onLogin(true, credentials.email);

            navigate("/");
        } catch (error) {
            console.error("Login Error:", error);
            if (error.response?.status === 401) {
                setError("Invalid email or password!");
            } else {
                setError(error.message || "Something wrong happend");
            }
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <div className='w-screen h-screen flex flex-col items-center bg-content gap-4'>
            <h1 className='font-montserrat text-3xl text-bkg pt-5'>Log in to your account to discover our products</h1>

            <div className="w-1/2 min-h-[20rem] h-max rounded-lg shadow-xl bg-content justify-center items-center gap-2 flex flex-col ">
                {error && <p className="text-red-500 mb-4">{error}</p>}

                <form onSubmit={handleSubmit} className="mt-3 w-full max-w-md flex flex-col gap-2 pb-3 justify-center items-center">
                    <input
                        type="text"
                        name="email"
                        placeholder="Email"
                        value={credentials.email}
                        onChange={handleChange}
                        className="w-1/2 p-2 border-navborder border bg-greyInput rounded text-grey"
                    />

                    <input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={credentials.password}
                        onChange={handleChange}
                        className="w-1/2 p-2 border-navborder border bg-greyInput rounded text-grey"
                    />

                    <p className="text-lg text-grey">
                        Don't have an account?{" "}
                        <Link to="/register" className="text-blue hover:underline">
                            join us here
                        </Link>
                    </p>

                    <button
                        type="submit"
                        className="flex w-[8.36781rem] h-[2.42938rem] bg-red-400 justify-center items-center shadow-md rounded-xl text-white"
                        disabled={isLoading}
                    >
                        {isLoading ? "Logging in..." : "Login"}
                    </button>
                </form>

            </div>



        </div>
    )
}
