const Order = require("../models/OrderModel");

async function createOrder({ productId, orderDate, quantity, orderPay }) {
  try {
    const newOrder = await Order.create({
      productId,
      orderDate,
      quantity,
      orderPay,
    });

    console.log("Commande créée avec succès");
    return newOrder;
  } catch (err) {
    console.error("Erreur lors de l'ajout de la commande :", err);
    throw new Error("Erreur lors de l'ajout de cette commande.");
  }
}

async function getOrderById(orderId) {
  try {
    const order = await Order.findById(orderId);

    if (!order) {
      throw new Error("Commande non trouvée");
    }

    console.log(order);
    return order;
  } catch (err) {
    console.error("Erreur lors de la récupération du commande :", err);
    throw new Error("Erreur lors de la récupération du commande.");
  }
}
async function getOrders() {
  try {
    const orders = await Order.find({}).exec();
    

    if (!orders) {
      throw new Error("can't get orders");
    }

    console.log(orders);
    return orders;
  } catch (err) {
    console.error("Erreur lors de la récupération du commande :", err);
    throw new Error("Erreur lors de la récupération du commande.");
  }
}

async function updateOrder(orderId, { productId, orderDate, quantity, orderPay }) {
  try {
    const updatedOrder = await Order.findByIdAndUpdate(
      orderId,
      { productId, orderDate, quantity, orderPay },
      { new: true }
    );
    const order = await Order.findByIdAndUpdate()

    if (!updatedOrder) {
      throw new Error("Commande non trouvée");
    }

    console.log("Commande mise à jour avec succès");
    console.log(updatedOrder)
    return updatedOrder;
  } catch (err) {
    console.error("Erreur lors de la mise à jour de la commande :", err);
    throw new Error("Erreur lors de la mise à jour de la commande.");
  }
}

module.exports = {
  createOrder,
  getOrderById,
  updateOrder,
  getOrders,
};
