import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import AuthenticationService from "../services/AuthenticationService";

const Register = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [password, setPassword] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!firstName || !lastName || !email || !phone || !password) {
            alert("All fields are required");
            return;
        }

        try {
            await AuthenticationService.register({
                email: email,
                password: password,
                firstName: firstName,
                lastName: lastName,
                phone: phone,
            });

            alert("User added successfully!");
            navigate("/login");
        } catch (error) {
            alert(error.message);
        }
    };

    return (
        <div className="w-screen h-screen flex flex-col items-center bg-content gap-4">

            <h1 className='font-montserrat text-3xl text-bkg pt-5'>Create an account and discover our magnificant gadgets</h1>

            <div className="w-1/2 min-h-[20rem] h-max rounded-lg shadow-xl bg-content justify-center items-center gap-2 flex flex-col ">
                <form onSubmit={handleSubmit} className="mt-3 w-full max-w-md flex flex-col gap-2 pb-3 justify-center items-center">
                <input
                        type="text"
                        placeholder="Firstname"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                        className="w-1/2 p-2 border-navborder border bg-greyInput rounded text-grey"
                        required
                    />
                    <input
                        type="text"
                        placeholder="Lastname"
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                        className="w-1/2 p-2 border-navborder border bg-greyInput rounded text-grey"
                        required
                    />
                    <input
                        type="text"
                        placeholder="Email Address"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        className="w-1/2 p-2 border-navborder border bg-greyInput rounded text-grey"
                        required
                    />
                    <input
                        type="text"
                        placeholder="Phone"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        className="w-1/2 p-2 border-navborder border bg-greyInput rounded text-grey"
                        required
                    />
                    <input
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        className="w-1/2 p-2 border-navborder border bg-greyInput rounded text-grey"
                        required
                    />

                    <p className="text-lg text-grey">
                        Already have an account?{" "}
                        <Link to="/login" className="text-blue hover:underline">
                            Login
                        </Link>
                    </p>

                    <button
                        type="submit"
                        className="flex w-[8.36781rem] h-[2.42938rem] bg-red-400 justify-center items-center shadow-md rounded-xl text-white"
                        disabled={isLoading}
                    >
                        {isLoading ? "Registering ..." : "Register"}
                    </button>
                </form>
                
            </div>
        </div>
    );
};

export default Register;